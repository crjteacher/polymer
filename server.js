var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
app.use(express.static(__dirname + '/build/default'))
app.listen(port);

console.log('Waiting for Polymer-Node: ' + port);

app.get("/", function (req, res) {
  res.sendFile("index.html", {root:'.'});
});

app.get("/clientes/:idcliente", function (req, res) {
  res.send("Cliente enviado: " + req.params.idcliente);
});

app.post("/", function(req, res) {
  res.send("Recibí el post actualizada");
});

app.delete("/", function(req, res) {
  res.send("{\"msg\": \"Elemento eliminado exitosamente\"}")
});

app.put("/", function(req, res) {
  res.send("{\"msg\": \"Elemento actualizado exitosamente\"}")
});
